const EventEmitter = {
  events: new Map(),
  listen: (topic, callback) => {
    const oldEvents = EventEmitter.events.get(topic);
    if (EventEmitter.events.has(topic)) {
      return EventEmitter.events.set(topic, [...oldEvents, callback]);
    }
    return EventEmitter.events.set(topic, [callback]);
  },
  emit: (topic, data) => {
    const myListeners = EventEmitter.events.get(topic);
    if (Array.isArray(myListeners) && myListeners.length) {
      myListeners.forEach((event) => event(data));
    }
  },
};

export default EventEmitter;
