"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var EventEmitter = {
  events: new Map(),
  listen: function listen(topic, callback) {
    var oldEvents = EventEmitter.events.get(topic);

    if (EventEmitter.events.has(topic)) {
      return EventEmitter.events.set(topic, [].concat((0, _toConsumableArray2.default)(oldEvents), [callback]));
    }

    return EventEmitter.events.set(topic, [callback]);
  },
  emit: function emit(topic, data) {
    var myListeners = EventEmitter.events.get(topic);

    if (Array.isArray(myListeners) && myListeners.length) {
      myListeners.forEach(function (event) {
        return event(data);
      });
    }
  }
};
var _default = EventEmitter;
exports.default = _default;